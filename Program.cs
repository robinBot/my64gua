﻿// See https://aka.ms/new-console-template for more information
using System.Text;

///输入参数:1~3，输出生成的本卦、之卦

///不输入参数，默认返回八卦、六十四卦.
Console.OutputEncoding = Encoding.UTF8;


if (args.Length > 0)
{
   int 生成卦数量 = 0;
   try
   {
      生成卦数量 = int.Parse(args[0]);
   }
   catch (Exception e)
   {
      Console.WriteLine("出错了，请输入数字参数（1~3）。" + e.Message);
   }
   if (生成卦数量 > 3)
   {
      Console.WriteLine("不能超过3卦哟，太贪心了:)");
      return;
   }
   if (生成卦数量 < 0)
   {
      Console.WriteLine("您是要替我生成几个卦吗？请输入数字参数（1~3）");
   }
   {
      if (生成卦数量 == 0)
      {
         Console.WriteLine(易经.八卦.格式输出());
         Console.WriteLine(易经.六十四卦.格式输出());
      }
      for (int i = 0; i < 生成卦数量; i++)
      {
         Console.WriteLine(易经.筮法.生成一卦());
      }
   }

}
else
{
   Console.WriteLine(易经.八卦.格式输出());
   Console.WriteLine(易经.六十四卦.格式输出());
}


