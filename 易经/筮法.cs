using System.Linq;

namespace 易经;

/// <summary>
/// 参考书目：廖名春《周易经传十五讲》北京大学出版社 2006年第4次印刷，p54页 64卦的产生，简易算法
/// </summary>
public class 筮法
{
    private const int 蓍草数 = 48; //49-1=55-6-1
    static private readonly Random r = new();
    static public 阴阳[] 六爻 = new 阴阳[6];//存储生成卦的六爻，0为初爻，1为二爻，...4为5爻,5为上爻.

    /// <summary>
    /// 假如一个爻是老阴或者老阳，那么这个爻就是动爻,由此生成之卦(变卦)
    /// </summary>
    /// <returns></returns>
    static public string 生成一卦()
    {
        string result = "";

        //六爻
        for (int i = 0; i < 6; i++)
        {
            //三变
            int s = 0;//三变随机数之和
            for (int j = 0; j <= 2; j++)
            {
                int n = r.Next(2) == 0 ? 4 : 8;
                s += n;
            }

            int 爻数 = (蓍草数 - s) / 4;
            六爻[i] = (阴阳)爻数;
        }

        卦[] 卦64 = 六十四卦.列表;

        int 本卦卦数 = 0;
        for (int i = 0; i < 6; i++)
        {
            本卦卦数 += (int)六爻[i] % 2 * (1 << i);  //(1<<i)表示用移位方式计算2的i次幂
        }

        卦 本卦 = 卦64.Where(g => g.卦数 == 本卦卦数).First();//根据卦数找到对应的卦
        result = "本卦\n";
        result += 格式输出(本卦, 六爻);


        int 之卦卦数 = 0;
        阴阳[] 之卦六爻 = new 阴阳[6];
        for (int i = 0; i < 6; i++)
        {
            之卦六爻[i] = 六爻[i];
            if (六爻[i] == 阴阳.少阴) 之卦六爻[i] = 阴阳.老阴;
            if (六爻[i] == 阴阳.少阳) 之卦六爻[i] = 阴阳.老阳;

            if (六爻[i] == 阴阳.老阴) 之卦六爻[i] = 阴阳.少阳;
            if (六爻[i] == 阴阳.老阳) 之卦六爻[i] = 阴阳.少阴;


            之卦卦数 += (int)之卦六爻[i] % 2 * (1 << i);  //(1<<i)表示用移位方式计算2的i次幂
        }


        result += "之卦（变卦）\n";

        卦 之卦 = 卦64.Where(g => g.卦数 == 之卦卦数).First();//根据卦数找到对应的卦
        result += 格式输出(之卦, 之卦六爻);
        return result;
    }



    /// <summary>
    /// 输出生成卦的内容
    /// </summary>
    /// <returns>卦名，符号，（从下到上的六爻，少阳=7，老阳=9，少阴8，老阴=6）.本卦的象数理</returns>
    static public string 格式输出(卦 参数卦, 阴阳[] 参数爻)
    {


        string result = "";

        //卦 当前卦 = 卦64.Where(g=>g.卦数==当前卦数).First();//根据卦数找到对应的卦
        卦 当前卦 = 参数卦;
        string 爻 = string.Join(", ", 六爻);
        if (参数爻 != null)
        {
            爻 = string.Join(", ", 参数爻);
        }

        result = $"【{当前卦.卦符号} 】 {当前卦.卦名}  ({爻})\n卦象:{当前卦.卦象},卦数:{当前卦.卦数},卦理:{当前卦.卦理}\n";

        return result;
    }

}